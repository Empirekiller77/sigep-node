using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SIGEP.Controllers;
using SIGEP.Models;
using SIGEPTests1.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class UnitTest1
    {

        [Fact]
        public void HomePageTest()
        {
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["Key"] = "test";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            Controller controller = new HomeController(null);
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
           
            Assert.Equal(mockSession["Key"], controller.ControllerContext.HttpContext.Session.GetString("Key"));
        }
    }
}
