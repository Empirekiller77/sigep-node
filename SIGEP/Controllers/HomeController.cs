﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIGEP.Data;
using SIGEP.models;
using SIGEP.Models;

namespace SIGEP.Controllers
{
    public class HomeController : Controller
    {
        public string SessionKeyEmail = "_Email";
        public string SessionKeyPassword = "_Password";
        public string SessionKeyUserType = "_UserType";
        const string SessionKeyTime = "_Time";
        public string SessionInfo_Name { get; private set; }
        public string SessionInfo_Age { get; private set; }
        public string SessionInfo_CurrentTime { get; private set; }
        public string SessionInfo_SessionTime { get; private set; }
        public string SessionInfo_MiddlewareValue { get; private set; }
        private readonly ILogger<HomeController> _logger;

        private SWDBContext _context;

        public HomeController(ILogger<HomeController> logger, SWDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Homepage()
        {
            DataBaseUtils utils = new DataBaseUtils(_context);
            string email = HttpContext.Request.Form["email"];
            string password = HttpContext.Request.Form["password"];
            string encrypt = DataBaseUtils.encryptPasswordUser(password);
            var query = from a in _context.Users where a.Email.Equals(email) && a.Password.Equals(encrypt) select a;
            Users user = query.FirstOrDefault();
            if (query.Count() > 0)
            {
                HttpContext.Session.SetString(SessionKeyEmail, email);
                HttpContext.Session.SetString(SessionKeyPassword, password);
                ViewBag.name = user.Name;
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult LoginTry()
        {
            return Json(new string[] { "haha it returns things" });
        }

        [HttpGet]
        public IActionResult RecoverPassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult RecoverPassword(RecoverPassword model)
        {
            if (_context.Users.Any(e => e.Email.Equals(model.Email)))
            {
                var token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                var passwordResetLink = Url.Action("ResetPassword", "Home",
                    new { email = model.Email, token = token}, Request.Scheme);
                DataBaseUtils.SendEmailRecoveryPassword(passwordResetLink, model.Email);

                
                return View("RecoverPasswordConfirmation");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult ResetPassword(string email, string token)
        {
           if (token==null || email==null) {
                ModelState.AddModelError("","Invalid Token");
            }
            return View();
        }

        [HttpPost]
        public IActionResult ResetPassword(ResetPassword model)
        {
            string encryptPassword = DataBaseUtils.encryptPasswordUser(model.Password);
            Users user = _context.Users.Where(b => b.Email.Equals(model.Email)).ToArray()[0];
            user.Password = encryptPassword;
            _context.Update(user);
            _context.SaveChangesAsync();
            return View();
        }

        public IActionResult Contacts()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View();
        }
    }
}
