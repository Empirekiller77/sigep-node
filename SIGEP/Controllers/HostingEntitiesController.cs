﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SIGEP.models;

namespace SIGEP.Controllers
{
    public class HostingEntitiesController : Controller
    {
        private readonly SWDBContext _context;

        public HostingEntitiesController(SWDBContext context)
        {
            _context = context;
        }

        // GET: HostingEntities
        public async Task<IActionResult> Index()
        {
            var sWDBContext = _context.HostingEntity.Include(h => h.UserRepNavigation);
            return View(await sWDBContext.ToListAsync());
        }

        // GET: HostingEntities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hostingEntity = await _context.HostingEntity
                .Include(h => h.UserRepNavigation)
                .FirstOrDefaultAsync(m => m.IdHostingEntity == id);
            if (hostingEntity == null)
            {
                return NotFound();
            }

            return View(hostingEntity);
        }

        // GET: HostingEntities/Create
        public IActionResult Create()
        {
            //query to check representative type missing
            ViewData["UserRep"] = new SelectList(_context.Users, "UserNumber", "Name");
            return View();
        }

        // POST: HostingEntities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdHostingEntity,Name,Telephone,Email,Address,UserRep")] HostingEntity hostingEntity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hostingEntity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //query to check representative type missing
            ViewData["UserRep"] = new SelectList(_context.Users, "UserNumber", "Name", hostingEntity.UserRep);
            return View(hostingEntity);
        }

        // GET: HostingEntities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hostingEntity = await _context.HostingEntity.FindAsync(id);
            if (hostingEntity == null)
            {
                return NotFound();
            }
            //query to check representative type missing
            ViewData["UserRep"] = new SelectList(_context.Users, "UserNumber", "Name", hostingEntity.UserRep);
            return View(hostingEntity);
        }

        // POST: HostingEntities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdHostingEntity,Name,Telephone,Email,Address,UserRep")] HostingEntity hostingEntity)
        {
            if (id != hostingEntity.IdHostingEntity)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hostingEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HostingEntityExists(hostingEntity.IdHostingEntity))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //query to check representative type missing
            ViewData["UserRep"] = new SelectList(_context.Users, "UserNumber", "Name", hostingEntity.UserRep);
            return View(hostingEntity);
        }

        // GET: HostingEntities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hostingEntity = await _context.HostingEntity
                .Include(h => h.UserRepNavigation)
                .FirstOrDefaultAsync(m => m.IdHostingEntity == id);
            if (hostingEntity == null)
            {
                return NotFound();
            }

            return View(hostingEntity);
        }

        // POST: HostingEntities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hostingEntity = await _context.HostingEntity.FindAsync(id);
            _context.HostingEntity.Remove(hostingEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HostingEntityExists(int id)
        {
            return _context.HostingEntity.Any(e => e.IdHostingEntity == id);
        }
    }
}
