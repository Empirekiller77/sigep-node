﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SIGEP.models;

namespace SIGEP.Controllers
{
    public class StudentTasksController : Controller
    {
        private readonly SWDBContext _context;

        public StudentTasksController(SWDBContext context)
        {
            _context = context;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddDiaryTask([Bind("IdTask,IdStudent,Title,StartDate,TaskEndDate, Description")] StudentTask task)
        {
            if (ModelState.IsValid)
            {
                _context.Add(task);
                await _context.SaveChangesAsync();
                return RedirectToAction("Perfil", "Users");
            }
            return RedirectToAction("Perfil", "Users");
        }

        [HttpGet]
        public async Task<IActionResult> DeleteTask(int id)
        {
            Console.WriteLine("IDDDDDDDDDDDD " + id);
            var task = await _context.StudentTask.FindAsync(id);
         
            _context.StudentTask.Remove(task);
            await _context.SaveChangesAsync();
            return RedirectToAction("Perfil", "Users");
        }

        // GET: HostingEntities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var task = await _context.StudentTask.FindAsync(id);
            if (task == null)
            {
                return NotFound();
            }

            return View(task);
        }

        // POST: HostingEntities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTask,IdStudent,Title,StartDate,TaskEndDate, Description")] StudentTask task)
        {
            if (id != task.IdTask)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(task);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentTaskExists(task.IdTask))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("Perfil");
        }
        private bool StudentTaskExists(int id)
        {
            return _context.StudentTask.Any(e => e.IdTask == id);
        }
    }
}
