﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SIGEP.models;

namespace SIGEP.Controllers
{
    public class ActivitiesController : Controller
    {
        private readonly SWDBContext _context;

        public ActivitiesController(SWDBContext context)
        {
            _context = context;
        }

        // GET: Activities
        public async Task<IActionResult> Index()
        {
            var sWDBContext = _context.Activities.Include(a => a.IdActivityTypesNavigation).Include(a => a.IdHostingEntityNavigation).Include(a => a.IdStudentNavigation);
            return View(await sWDBContext.ToListAsync());
        }

        // GET: Activities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activities = await _context.Activities
                .Include(a => a.IdActivityTypesNavigation)
                .Include(a => a.IdHostingEntityNavigation)
                .Include(a => a.IdStudentNavigation)
                .FirstOrDefaultAsync(m => m.IdActivity == id);
            if (activities == null)
            {
                return NotFound();
            }

            return View(activities);
        }

        // GET: Activities/Create
        public IActionResult Create()
        {
            ViewData["IdActivityTypes"] = new SelectList(_context.ActivityTypes, "IdType", "Type");
            ViewData["IdHostingEntity"] = new SelectList(_context.HostingEntity, "IdHostingEntity", "Name");
            ViewData["IdStudent"] = new SelectList(_context.Users, "UserNumber", "Name");
            return View();
        }

        // POST: Activities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdActivity,IdHostingEntity,IdActivityTypes,IdStudent,Title,VacancyLimit,Location,MainRequirements,ConfidentialityRequirements,UsedTechs,Observations,Description,Objectives,WorkTime")] Activities activities)
        {
            if (ModelState.IsValid)
            {
                _context.Add(activities);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdActivityTypes"] = new SelectList(_context.ActivityTypes, "IdType", "Type", activities.IdActivityTypes);
            ViewData["IdHostingEntity"] = new SelectList(_context.HostingEntity, "IdHostingEntity", "Address", activities.IdHostingEntity);
            ViewData["IdStudent"] = new SelectList(_context.Users, "UserNumber", "Address", activities.IdStudent);
            return View(activities);
        }

        // GET: Activities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activities = await _context.Activities.FindAsync(id);
            if (activities == null)
            {
                return NotFound();
            }
            ViewData["IdActivityTypes"] = new SelectList(_context.ActivityTypes, "IdType", "Type", activities.IdActivityTypes);
            ViewData["IdHostingEntity"] = new SelectList(_context.HostingEntity, "IdHostingEntity", "Address", activities.IdHostingEntity);
            ViewData["IdStudent"] = new SelectList(_context.Users, "UserNumber", "Address", activities.IdStudent);
            return View(activities);
        }

        // POST: Activities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdActivity,IdHostingEntity,IdActivityTypes,IdStudent,Title,VacancyLimit,Location,MainRequirements,ConfidentialityRequirements,UsedTechs,Observations,Description,Objectives,WorkTime")] Activities activities)
        {
            if (id != activities.IdActivity)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(activities);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActivitiesExists(activities.IdActivity))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdActivityTypes"] = new SelectList(_context.ActivityTypes, "IdType", "Type", activities.IdActivityTypes);
            ViewData["IdHostingEntity"] = new SelectList(_context.HostingEntity, "IdHostingEntity", "Address", activities.IdHostingEntity);
            ViewData["IdStudent"] = new SelectList(_context.Users, "UserNumber", "Address", activities.IdStudent);
            return View(activities);
        }

        // GET: Activities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activities = await _context.Activities
                .Include(a => a.IdActivityTypesNavigation)
                .Include(a => a.IdHostingEntityNavigation)
                .Include(a => a.IdStudentNavigation)
                .FirstOrDefaultAsync(m => m.IdActivity == id);
            if (activities == null)
            {
                return NotFound();
            }

            return View(activities);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var activities = await _context.Activities.FindAsync(id);
            _context.Activities.Remove(activities);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ActivitiesExists(int id)
        {
            return _context.Activities.Any(e => e.IdActivity == id);
        }
    }
}
