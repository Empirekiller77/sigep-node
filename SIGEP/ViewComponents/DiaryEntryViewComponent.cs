﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SIGEP.models;


namespace SIGEP.ViewComponents
{
    public class DiaryEntryViewComponent : ViewComponent
    {
       
        private SWDBContext _context;

        public DiaryEntryViewComponent( SWDBContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            string email = HttpContext.Session.GetString("_Email");
            var user = _context.Users.Where(u => u.Email.Equals(email)).FirstOrDefault();
            return View(_context.StudentTask.Where(t => t.IdStudent == user.UserNumber));
        }



    }
}
