﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SIGEP.models
{
    public partial class HostingEntity
    {
        public HostingEntity()
        {
            Activities = new HashSet<Activities>();
            Message = new HashSet<Message>();
        }

        public int IdHostingEntity { get; set; }

   
        [Required]
        [RegularExpression(@"[a-zA-Z\s]+$", ErrorMessage = "Nome Inválido")]
        [MinLength(8, ErrorMessage = "Nome Demasiado Curto")]
        [MaxLength(40, ErrorMessage = "Nome Demasiado Longo")]
        public string Name { get; set; }

        [Required]
        [RegularExpression("^[0-9]{9}$", ErrorMessage = "Telefone Inválido") ]
        public int? Telephone { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Email Inválido")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MinLength(8, ErrorMessage = "Morada Demasiado Curta")]
        [MaxLength(45, ErrorMessage = "Morada Demasiado Longa")]
        public string Address { get; set; }

        [RegularExpression("[0-9]{1,}")]
        [Display(Name="Representante da Entidade")]
        public int? UserRep { get; set; }

        public virtual Users UserRepNavigation { get; set; }
        public virtual ICollection<Activities> Activities { get; set; }
        public virtual ICollection<Message> Message { get; set; }
    }
}
