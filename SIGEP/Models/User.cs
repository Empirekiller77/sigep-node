﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIGEP.Models
{
    public abstract class User
    {
        [Key]
        public int user_number { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public int user_type { get; set; }
        //public List<Notification> notification; -> to be created
        //public List<Message> notification; -> to be created
        //public List<proposal> notification; -> to be created

        public User(int user_number, string name, string address, int user_type)
        {
            this.user_number = user_number;
            this.name = name;
            this.address = address;
            this.email = construct_email(this);
            this.password = "hello";
            this.user_type = user_type;
        }

        public User(int user_number, string name, string address, string email, int user_type)
        {
            this.user_number = user_number;
            this.name = name;
            this.address = address;
            this.email = email;
            this.password = "";
            this.user_type = user_type;
        }

        protected void db_insert_user(User user)
        {
            //connect to data bse and insert a new user
        }

        private string construct_email(User user)
        {
            if (user.GetType() == typeof(Student))
                return this.user_number + "@estudantes.ips.pt";

            return null;
        }
        /*
        public int db_get_userId()
        {
            
        }
        */
      

    

    }
}
