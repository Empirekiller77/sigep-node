﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SIGEP.models
{
    public partial class SWDBContext : DbContext
    {
        public SWDBContext()
        {
        }

        public SWDBContext(DbContextOptions<SWDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Activities> Activities { get; set; }
        public virtual DbSet<ActivityDocuments> ActivityDocuments { get; set; }
        public virtual DbSet<ActivityProposal> ActivityProposal { get; set; }
        public virtual DbSet<ActivityTypes> ActivityTypes { get; set; }
        public virtual DbSet<DocumentTypes> DocumentTypes { get; set; }
        public virtual DbSet<EvaluationApproval> EvaluationApproval { get; set; }
        public virtual DbSet<EvaluationJuri> EvaluationJuri { get; set; }
        public virtual DbSet<FinalEvaluation> FinalEvaluation { get; set; }
        public virtual DbSet<HostingEntity> HostingEntity { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<NotificationFormat> NotificationFormat { get; set; }
        public virtual DbSet<Notifications> Notifications { get; set; }
        public virtual DbSet<ProposalCandidate> ProposalCandidate { get; set; }
        public virtual DbSet<StudentAdvisor> StudentAdvisor { get; set; }
        public virtual DbSet<StudentTask> StudentTask { get; set; }
        public virtual DbSet<Survey> Survey { get; set; }
        public virtual DbSet<SurveyAnswers> SurveyAnswers { get; set; }
        public virtual DbSet<UserTypes> UserTypes { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=LAPTOP-TU53V2D4;Initial Catalog=SWDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activities>(entity =>
            {
                entity.HasKey(e => e.IdActivity)
                    .HasName("PK__activiti__BBF4A247A8366CAB");

                entity.ToTable("activities", "SWS");

                entity.HasIndex(e => e.IdActivityTypes)
                    .HasName("fk_activities_activity_types1_idx");

                entity.HasIndex(e => e.IdHostingEntity)
                    .HasName("fk_internships_hosting_entity1_idx");

                entity.HasIndex(e => e.IdStudent)
                    .HasName("fk_activities_users1_idx");

                entity.Property(e => e.IdActivity).HasColumnName("id_activity");

                entity.Property(e => e.ConfidentialityRequirements)
                    .HasColumnName("confidentiality_requirements")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdActivityTypes).HasColumnName("id_activity_types");

                entity.Property(e => e.IdHostingEntity).HasColumnName("id_hosting_entity");

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.MainRequirements)
                    .IsRequired()
                    .HasColumnName("main_requirements")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Objectives)
                    .IsRequired()
                    .HasColumnName("objectives")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Observations)
                    .HasColumnName("observations")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.UsedTechs)
                    .IsRequired()
                    .HasColumnName("used_techs")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.VacancyLimit).HasColumnName("vacancy_limit");

                entity.Property(e => e.WorkTime).HasColumnName("work_time");

                entity.HasOne(d => d.IdActivityTypesNavigation)
                    .WithMany(p => p.Activities)
                    .HasForeignKey(d => d.IdActivityTypes)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_activities_activity_types1");

                entity.HasOne(d => d.IdHostingEntityNavigation)
                    .WithMany(p => p.Activities)
                    .HasForeignKey(d => d.IdHostingEntity)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_internships_hosting_entity1");

                entity.HasOne(d => d.IdStudentNavigation)
                    .WithMany(p => p.Activities)
                    .HasForeignKey(d => d.IdStudent)
                    .HasConstraintName("fk_activities_users1");
            });

            modelBuilder.Entity<ActivityDocuments>(entity =>
            {
                entity.HasKey(e => e.ActivitiesIdActivity)
                    .HasName("PK__activity__9AB793EF56842CDF");

                entity.ToTable("activity_documents", "SWS");

                entity.HasIndex(e => e.ActivitiesIdActivity)
                    .HasName("fk_activity_documents_activities1_idx");

                entity.HasIndex(e => e.IdDocumentTypes)
                    .HasName("fk_activity_documents_document_types1_idx");

                entity.Property(e => e.ActivitiesIdActivity)
                    .HasColumnName("activities_id_activity")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateOfUpload)
                    .HasColumnName("date_of_upload")
                    .HasColumnType("date");

                entity.Property(e => e.IdDocumentTypes).HasColumnName("id_document_types");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnName("path")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.ActivitiesIdActivityNavigation)
                    .WithOne(p => p.ActivityDocuments)
                    .HasForeignKey<ActivityDocuments>(d => d.ActivitiesIdActivity)
                    .HasConstraintName("fk_activity_documents_activities1");

                entity.HasOne(d => d.IdDocumentTypesNavigation)
                    .WithMany(p => p.ActivityDocuments)
                    .HasForeignKey(d => d.IdDocumentTypes)
                    .HasConstraintName("fk_activity_documents_document_types1");
            });

            modelBuilder.Entity<ActivityProposal>(entity =>
            {
                entity.HasKey(e => e.ActivitiesIdActivity)
                    .HasName("PK__activity__9AB793EFCC9AA639");

                entity.ToTable("activity_proposal", "SWS");

                entity.HasIndex(e => e.ActivitiesIdActivity)
                    .HasName("fk_proposal_activities1_idx");

                entity.HasIndex(e => e.UsersIdUser)
                    .HasName("fk_proposal_users1_idx");

                entity.Property(e => e.ActivitiesIdActivity)
                    .HasColumnName("activities_id_activity")
                    .ValueGeneratedNever();

                entity.Property(e => e.Approved).HasColumnName("approved");

                entity.Property(e => e.UsersIdUser).HasColumnName("users_id_user");

                entity.HasOne(d => d.ActivitiesIdActivityNavigation)
                    .WithOne(p => p.ActivityProposal)
                    .HasForeignKey<ActivityProposal>(d => d.ActivitiesIdActivity)
                    .HasConstraintName("fk_proposal_activities1");

                entity.HasOne(d => d.UsersIdUserNavigation)
                    .WithMany(p => p.ActivityProposal)
                    .HasForeignKey(d => d.UsersIdUser)
                    .HasConstraintName("fk_proposal_users1");
            });

            modelBuilder.Entity<ActivityTypes>(entity =>
            {
                entity.HasKey(e => e.IdType)
                    .HasName("PK__activity__C3F091E00F136DEB");

                entity.ToTable("activity_types", "SWS");

                entity.Property(e => e.IdType).HasColumnName("id_type");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DocumentTypes>(entity =>
            {
                entity.HasKey(e => e.IdDocumentType)
                    .HasName("PK__document__3751293B4FC659CE");

                entity.ToTable("document_types", "SWS");

                entity.Property(e => e.IdDocumentType).HasColumnName("id_document_type");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EvaluationApproval>(entity =>
            {
                entity.HasKey(e => new { e.IdParticipant, e.IdEvaluation })
                    .HasName("PK__evaluati__1082C4B7A480CBCF");

                entity.ToTable("evaluation_approval", "SWS");

                entity.HasIndex(e => e.IdEvaluation)
                    .HasName("fk_evaluation_approval_final_evaluation1_idx");

                entity.HasIndex(e => e.IdParticipant)
                    .HasName("fk_evaluation_approval_users1_idx");

                entity.Property(e => e.IdParticipant).HasColumnName("id_participant");

                entity.Property(e => e.IdEvaluation).HasColumnName("id_evaluation");

                entity.Property(e => e.Approved).HasColumnName("approved");

                entity.HasOne(d => d.IdEvaluationNavigation)
                    .WithMany(p => p.EvaluationApproval)
                    .HasForeignKey(d => d.IdEvaluation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_evaluation_approval_final_evaluation1");

                entity.HasOne(d => d.IdParticipantNavigation)
                    .WithMany(p => p.EvaluationApproval)
                    .HasForeignKey(d => d.IdParticipant)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_evaluation_approval_users1");
            });

            modelBuilder.Entity<EvaluationJuri>(entity =>
            {
                entity.HasKey(e => new { e.IdJuriMember, e.IdFinalEvaluation })
                    .HasName("PK__evaluati__D75463CBCE983777");

                entity.ToTable("evaluation_juri", "SWS");

                entity.HasIndex(e => e.IdFinalEvaluation)
                    .HasName("fk_evaluation_juri_final_evaluation1_idx");

                entity.HasIndex(e => e.IdJuriMember)
                    .HasName("fk_evaluation_juri_users1_idx");

                entity.Property(e => e.IdJuriMember).HasColumnName("id_juri_member");

                entity.Property(e => e.IdFinalEvaluation).HasColumnName("id_final_evaluation");

                entity.HasOne(d => d.IdFinalEvaluationNavigation)
                    .WithMany(p => p.EvaluationJuri)
                    .HasForeignKey(d => d.IdFinalEvaluation)
                    .HasConstraintName("fk_evaluation_juri_final_evaluation1");

                entity.HasOne(d => d.IdJuriMemberNavigation)
                    .WithMany(p => p.EvaluationJuri)
                    .HasForeignKey(d => d.IdJuriMember)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_evaluation_juri_users1");
            });

            modelBuilder.Entity<FinalEvaluation>(entity =>
            {
                entity.HasKey(e => e.IdEvaluation)
                    .HasName("PK__final_ev__0B77AA11849945B2");

                entity.ToTable("final_evaluation", "SWS");

                entity.HasIndex(e => e.IdStudent)
                    .HasName("fk_final_evaluation_users1_idx");

                entity.Property(e => e.IdEvaluation).HasColumnName("id_evaluation");

                entity.Property(e => e.Approved).HasColumnName("approved");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Grade).HasColumnName("grade");

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.Record)
                    .HasColumnName("record")
                    .HasMaxLength(260)
                    .IsUnicode(false);

                entity.Property(e => e.Room)
                    .IsRequired()
                    .HasColumnName("room")
                    .HasMaxLength(45);

                entity.HasOne(d => d.IdStudentNavigation)
                    .WithMany(p => p.FinalEvaluation)
                    .HasForeignKey(d => d.IdStudent)
                    .HasConstraintName("fk_final_evaluation_users1");
            });

            modelBuilder.Entity<HostingEntity>(entity =>
            {
                entity.HasKey(e => e.IdHostingEntity)
                    .HasName("PK__hosting___4C13D353E95DB7F4");

                entity.ToTable("hosting_entity", "SWS");

                entity.HasIndex(e => e.UserRep)
                    .HasName("fk_hosting_entity_users1_idx");

                entity.Property(e => e.IdHostingEntity).HasColumnName("id_hosting_entity");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone).HasColumnName("telephone");

                entity.Property(e => e.UserRep).HasColumnName("user_rep");

                entity.HasOne(d => d.UserRepNavigation)
                    .WithMany(p => p.HostingEntity)
                    .HasForeignKey(d => d.UserRep)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_hosting_entity_users1");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.HasKey(e => e.IdMessage)
                    .HasName("PK__message__460F3CF43ADBC10D");

                entity.ToTable("message", "SWS");

                entity.HasIndex(e => e.HostingEntityIdHostingEntity)
                    .HasName("fk_message_hosting_entity1_idx");

                entity.HasIndex(e => e.UsersUserNumber)
                    .HasName("fk_message_users1_idx");

                entity.Property(e => e.IdMessage).HasColumnName("id_message");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("content")
                    .HasColumnType("text");

                entity.Property(e => e.HostingEntityIdHostingEntity).HasColumnName("hosting_entity_id_hosting_entity");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.UsersUserNumber).HasColumnName("users_user_number");

                entity.HasOne(d => d.HostingEntityIdHostingEntityNavigation)
                    .WithMany(p => p.Message)
                    .HasForeignKey(d => d.HostingEntityIdHostingEntity)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_message_hosting_entity1");

                entity.HasOne(d => d.UsersUserNumberNavigation)
                    .WithMany(p => p.Message)
                    .HasForeignKey(d => d.UsersUserNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_message_users1");
            });

            modelBuilder.Entity<NotificationFormat>(entity =>
            {
                entity.HasKey(e => e.IdNotificationFormat)
                    .HasName("PK__notifica__B5499E215F028CA3");

                entity.ToTable("notification_format", "SWS");

                entity.Property(e => e.IdNotificationFormat).HasColumnName("id_notification_format");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasColumnName("text")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<Notifications>(entity =>
            {
                entity.HasKey(e => e.IdNotification)
                    .HasName("PK__notifica__925C842F809C0407");

                entity.ToTable("notifications", "SWS");

                entity.HasIndex(e => e.IdNotificationFormat)
                    .HasName("fk_notifications_notification_format1_idx");

                entity.HasIndex(e => e.OriginUserNumber)
                    .HasName("fk_notifications_users1_idx");

                entity.HasIndex(e => e.ReceiverUserNumber)
                    .HasName("fk_notifications_users2_idx");

                entity.Property(e => e.IdNotification).HasColumnName("id_notification");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdNotificationFormat).HasColumnName("id_notification_format");

                entity.Property(e => e.MarkedRead).HasColumnName("marked_read");

                entity.Property(e => e.OriginUserNumber).HasColumnName("origin_user_number");

                entity.Property(e => e.ReceiverUserNumber).HasColumnName("receiver_user_number");

                entity.HasOne(d => d.IdNotificationFormatNavigation)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.IdNotificationFormat)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_notifications_notification_format1");

                entity.HasOne(d => d.OriginUserNumberNavigation)
                    .WithMany(p => p.NotificationsOriginUserNumberNavigation)
                    .HasForeignKey(d => d.OriginUserNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_notifications_users1");

                entity.HasOne(d => d.ReceiverUserNumberNavigation)
                    .WithMany(p => p.NotificationsReceiverUserNumberNavigation)
                    .HasForeignKey(d => d.ReceiverUserNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_notifications_users2");
            });

            modelBuilder.Entity<ProposalCandidate>(entity =>
            {
                entity.HasKey(e => new { e.IdActivity, e.IdStudent })
                    .HasName("PK__proposal__C94A8CFCE6D8A984");

                entity.ToTable("proposal_candidate", "SWS");

                entity.HasIndex(e => e.IdActivity)
                    .HasName("fk_proposal_candidate_activity_proposal1_idx");

                entity.HasIndex(e => e.IdStudent)
                    .HasName("fk_proposal_candidate_users1_idx");

                entity.Property(e => e.IdActivity).HasColumnName("id_activity");

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.Approved).HasColumnName("approved");

                entity.HasOne(d => d.IdActivityNavigation)
                    .WithMany(p => p.ProposalCandidate)
                    .HasForeignKey(d => d.IdActivity)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_proposal_candidate_activity_proposal1");

                entity.HasOne(d => d.IdStudentNavigation)
                    .WithMany(p => p.ProposalCandidate)
                    .HasForeignKey(d => d.IdStudent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_proposal_candidate_users1");
            });

            modelBuilder.Entity<StudentAdvisor>(entity =>
            {
                entity.HasKey(e => new { e.IdAdvisor, e.IdStudent })
                    .HasName("PK__student___2B8403C7DAA64E79");

                entity.ToTable("student_advisor", "SWS");

                entity.HasIndex(e => e.IdAdvisor)
                    .HasName("fk_student_advisor_activity_users2_idx");

                entity.HasIndex(e => e.IdStudent)
                    .HasName("fk_student_advisor_activity_users1_idx");

                entity.Property(e => e.IdAdvisor).HasColumnName("id_advisor");

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.Approved).HasColumnName("approved");

                entity.HasOne(d => d.IdAdvisorNavigation)
                    .WithMany(p => p.StudentAdvisorIdAdvisorNavigation)
                    .HasForeignKey(d => d.IdAdvisor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_student_advisor_activity_users2");

                entity.HasOne(d => d.IdStudentNavigation)
                    .WithMany(p => p.StudentAdvisorIdStudentNavigation)
                    .HasForeignKey(d => d.IdStudent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_student_advisor_activity_users1");
            });

            modelBuilder.Entity<StudentTask>(entity =>
            {
                entity.HasKey(e => e.IdTask)
                    .HasName("PK__student___C1D2C617B33CF35B");

                entity.ToTable("student_task", "SWS");

                entity.HasIndex(e => e.IdStudent)
                    .HasName("fk_student_task_users1_idx");

                entity.Property(e => e.IdTask).HasColumnName("id_task");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.IdStudent).HasColumnName("id_student");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TaskEndDate)
                    .HasColumnName("task_end_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(dateadd(day,(1),getdate()))");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdStudentNavigation)
                    .WithMany(p => p.StudentTask)
                    .HasForeignKey(d => d.IdStudent)
                    .HasConstraintName("fk_student_task_users1");
            });

            modelBuilder.Entity<Survey>(entity =>
            {
                entity.HasKey(e => e.IdSurvey)
                    .HasName("PK__survey__F6237DE76ACA7DDC");

                entity.ToTable("survey", "SWS");

                entity.Property(e => e.IdSurvey)
                    .HasColumnName("id_survey")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Deseja realizar Estágio ou Projeto?')");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SurveyEndDate)
                    .HasColumnName("survey_end_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(dateadd(day,(1),getdate()))");
            });

            modelBuilder.Entity<SurveyAnswers>(entity =>
            {
                entity.HasKey(e => e.SurveyIdSurvey)
                    .HasName("PK__survey_a__61D1C0FB16989916");

                entity.ToTable("survey_answers", "SWS");

                entity.HasIndex(e => e.SurveyIdSurvey)
                    .HasName("fk_survey_answers_survey1_idx");

                entity.HasIndex(e => e.UsersUserNumber)
                    .HasName("fk_survey_answers_users1_idx");

                entity.Property(e => e.SurveyIdSurvey)
                    .HasColumnName("survey_id_survey")
                    .ValueGeneratedNever();

                entity.Property(e => e.Answer).HasColumnName("answer");

                entity.Property(e => e.UsersUserNumber).HasColumnName("users_user_number");

                entity.HasOne(d => d.SurveyIdSurveyNavigation)
                    .WithOne(p => p.SurveyAnswers)
                    .HasForeignKey<SurveyAnswers>(d => d.SurveyIdSurvey)
                    .HasConstraintName("fk_survey_answers_survey1");

                entity.HasOne(d => d.UsersUserNumberNavigation)
                    .WithMany(p => p.SurveyAnswers)
                    .HasForeignKey(d => d.UsersUserNumber)
                    .HasConstraintName("fk_survey_answers_users1");
            });

            modelBuilder.Entity<UserTypes>(entity =>
            {
                entity.HasKey(e => e.IdType)
                    .HasName("PK__user_typ__C3F091E0114F70DB");

                entity.ToTable("user_types", "SWS");

                entity.Property(e => e.IdType).HasColumnName("id_type");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserNumber)
                    .HasName("PK__users__8E38467651DB1944");

                entity.ToTable("users", "SWS");

                entity.HasIndex(e => e.UserTypesIdType)
                    .HasName("fk_users_user_types1_idx");

                entity.Property(e => e.UserNumber)
                    .HasColumnName("user_number")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(128)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.UserTypesIdType)
                    .HasColumnName("user_types_id_type")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.UserTypesIdTypeNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.UserTypesIdType)
                    .HasConstraintName("fk_users_user_types1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
