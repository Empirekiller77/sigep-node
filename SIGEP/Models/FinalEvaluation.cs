﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class FinalEvaluation
    {
        public FinalEvaluation()
        {
            EvaluationApproval = new HashSet<EvaluationApproval>();
            EvaluationJuri = new HashSet<EvaluationJuri>();
        }

        public int IdEvaluation { get; set; }
        public int IdStudent { get; set; }
        public DateTime DateTime { get; set; }
        public string Room { get; set; }
        public int? Grade { get; set; }
        public string Record { get; set; }
        public bool? Approved { get; set; }

        public virtual Users IdStudentNavigation { get; set; }
        public virtual ICollection<EvaluationApproval> EvaluationApproval { get; set; }
        public virtual ICollection<EvaluationJuri> EvaluationJuri { get; set; }
    }
}
