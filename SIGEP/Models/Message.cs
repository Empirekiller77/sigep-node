﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class Message
    {
        public int IdMessage { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int UsersUserNumber { get; set; }
        public int HostingEntityIdHostingEntity { get; set; }

        public virtual HostingEntity HostingEntityIdHostingEntityNavigation { get; set; }
        public virtual Users UsersUserNumberNavigation { get; set; }
    }
}
