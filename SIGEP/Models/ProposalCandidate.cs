﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class ProposalCandidate
    {
        public int IdActivity { get; set; }
        public int IdStudent { get; set; }
        public bool? Approved { get; set; }

        public virtual ActivityProposal IdActivityNavigation { get; set; }
        public virtual Users IdStudentNavigation { get; set; }
    }
}
