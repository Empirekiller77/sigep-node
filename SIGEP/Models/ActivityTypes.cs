﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class ActivityTypes
    {
        public ActivityTypes()
        {
            Activities = new HashSet<Activities>();
        }

        public int IdType { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Activities> Activities { get; set; }
    }
}
