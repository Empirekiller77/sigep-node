Use SWDB;
GO
CREATE OR ALTER PROCEDURE [SWS].[sp_generateInsertProcedure] -- DROP OR ALTER
(
	@TABLE_NAME NVARCHAR(128), @SCHEMA_NAME nvarchar(128), @ParametersForProcedure nvarchar(MAX),
	  @ColumnsOfTable nvarchar(MAX), @VALUES nvarchar(MAX)
	  )
AS
BEGIN
	
	DECLARE @GeneratedSProcedure VARCHAR(MAX)

	-- INSERT SP
	SET @GeneratedSProcedure = CONCAT('
		CREATE PROCEDURE [', @SCHEMA_NAME, '].[sp_', @TABLE_NAME, '_insert]
		(', @ParametersForProcedure, ')
			AS
			BEGIN TRY
				INSERT INTO [', @SCHEMA_NAME, '].[', @TABLE_NAME, '] (', @ColumnsOfTable, ')
				VALUES (', @VALUES, ')
			END TRY
			BEGIN CATCH
				DECLARE @ErrorNumber INT
				DECLARE @Line INT
			
				SELECT @ErrorNumber = ERROR_NUMBER()
				SELECT @Line = ERROR_LINE()

				EXEC [sp_ErrorLogMessage] @ErrorNumber,@Line
			END CATCH
		');

		--Substitute procedure if one with the same name exists
			EXEC('DROP PROCEDURE IF EXISTS [' 
				+ @SCHEMA_NAME + '].[sp_' + @TABLE_NAME + '_insert]')

			EXEC(@GeneratedSProcedure)

END
-- -----------------------------------------------------------
-- ------------------GenerateUpdateProcedure------------------
GO
/****** Object:  StoredProcedure [SWS].[sp_generateUpdateProcedure]    Script Date: 15/11/2018 10:39:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [SWS].[sp_generateUpdateProcedure] --DROP OR ALTER
(
	@TABLE_NAME NVARCHAR(128), @SCHEMA_NAME nvarchar(128), @ParametersForProcedure nvarchar(MAX),
	 @ParamsForWhereClause nvarchar(MAX), @ColumnsOfTable nvarchar(MAX), @WhereClause nvarchar(MAX), @VALUES nvarchar(MAX),
		@SetClause NVARCHAR(MAX) 
	  )
AS
BEGIN
	
	DECLARE @GeneratedSProcedure VARCHAR(MAX)

	-- UPDATE SP
	SET @GeneratedSProcedure = CONCAT('
CREATE PROCEDURE [', @SCHEMA_NAME, '].[sp_', @TABLE_NAME, '_update]
(', @ParamsForWhereClause, ', ', @ParametersForProcedure, ')
AS
BEGIN TRY
	UPDATE [', @SCHEMA_NAME, '].[', @TABLE_NAME, ']
	SET ', @SetClause, '
	WHERE ', @WhereClause, '
END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT
	DECLARE @Line INT
			
	SELECT @ErrorNumber = ERROR_NUMBER()
	SELECT @Line = ERROR_LINE()

	EXEC [sp_ErrorLogMessage] @ErrorNumber,@Line
END CATCH
	');
	EXEC('DROP PROCEDURE IF EXISTS [' + @SCHEMA_NAME + '].[sp_' + @TABLE_NAME + '_update]')
	EXEC(@GeneratedSProcedure)


END
-- -----------------------------------------------------------
-- ------------------GenerateDeleteProcedure------------------
GO
/****** Object:  StoredProcedure [SWS].[sp_generateDeleteProcedure]    Script Date: 15/11/2018 10:40:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [SWS].[sp_generateDeleteProcedure] --DROP OR ALTER
(
	@TABLE_NAME NVARCHAR(128), @SCHEMA_NAME nvarchar(128),
	 @ParamsForWhereClause nvarchar(MAX), @WhereClause nvarchar(MAX)
	  )
AS
BEGIN
	
	DECLARE @GeneratedSProcedure VARCHAR(MAX)

	-- DELETE SP
	SET @GeneratedSProcedure = CONCAT('
CREATE PROCEDURE [', @SCHEMA_NAME, '].[sp_', @TABLE_NAME, '_delete]
(', @ParamsForWhereClause, ')
AS
BEGIN TRY
	DELETE FROM [', @SCHEMA_NAME, '].[', @TABLE_NAME, ']
	WHERE ', @WhereClause, '
END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT
	DECLARE @Line INT
			
	SELECT @ErrorNumber = ERROR_NUMBER()
	SELECT @Line = ERROR_LINE()

	EXEC [sp_ErrorLogMessage] @ErrorNumber,@Line
END CATCH
	');
	EXEC('DROP PROCEDURE IF EXISTS [' + @SCHEMA_NAME + '].[sp_' + @TABLE_NAME + '_delete]')
	EXEC(@GeneratedSProcedure)


END
-- -----------------------------------------------------------
-- ------------------GenerateProcedures-----------------------
GO
/****** Object:  StoredProcedure [SWS].[sp_generateProcedures]    Script Date: 15/11/2018 10:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [SWS].[sp_generateProcedures]
(
	@TABLE_NAME nvarchar(128)
)
AS
BEGIN
	DECLARE @COLUMN_NAME nvarchar(128)
	DECLARE @DATA_TYPE nvarchar(128)
	DECLARE @AllowedLength INT

	DECLARE @SCHEMA_NAME nvarchar(128) = (SELECT TOP 1 [TABLE_SCHEMA] FROM [INFORMATION_SCHEMA].[TABLES] WHERE [TABLE_NAME] = @TABLE_NAME)
	IF (@SCHEMA_NAME IS NULL)
	BEGIN
		SET @SCHEMA_NAME = SCHEMA_NAME()
	END

	DECLARE @ParamsForWhereClause nvarchar(MAX)
	DECLARE @WhereClause nvarchar(MAX)

	DECLARE CurQuery CURSOR FOR
		SELECT  [t].[COLUMN_NAME], [t].[DATA_TYPE], [t].[CHARACTER_MAXIMUM_LENGTH]
			FROM [INFORMATION_SCHEMA].[TABLE_CONSTRAINTS] [ts]
				INNER JOIN [INFORMATION_SCHEMA].[CONSTRAINT_COLUMN_USAGE] [con]
						ON [con].[TABLE_SCHEMA] = [ts].[TABLE_SCHEMA]
					AND [con].[TABLE_NAME] = [ts].[TABLE_NAME]
					AND [con].[CONSTRAINT_NAME] = [ts].[CONSTRAINT_NAME]
				INNER JOIN [INFORMATION_SCHEMA].[COLUMNS] [t]
						ON [t].[TABLE_SCHEMA] = [con].[TABLE_SCHEMA]
					AND [t].[TABLE_NAME] = [con].[TABLE_NAME]
					AND [t].[COLUMN_NAME] = [con].[COLUMN_NAME]
						WHERE [ts].[CONSTRAINT_TYPE] LIKE 'Primary Key'
					AND [ts].[TABLE_SCHEMA] LIKE @SCHEMA_NAME
					AND [ts].[TABLE_NAME] LIKE @TABLE_NAME

	OPEN CurQuery
		FETCH NEXT FROM CurQuery INTO @COLUMN_NAME, @DATA_TYPE, @AllowedLength
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (@AllowedLength IS NULL)
			BEGIN
				SET @ParamsForWhereClause = CONCAT(@ParamsForWhereClause, '@Where', @COLUMN_NAME, ' ', @DATA_TYPE, ', ')
			END
			ELSE IF (@AllowedLength = -1)
				BEGIN
					SET @ParamsForWhereClause = CONCAT(@ParamsForWhereClause, '@Where', @COLUMN_NAME, ' ', @DATA_TYPE, '(MAX)', ', ')
				END
			ELSE
				BEGIN
					SET @ParamsForWhereClause = CONCAT(@ParamsForWhereClause, '@Where', @COLUMN_NAME, ' ', @DATA_TYPE, '(', CONVERT(VARCHAR(10), @AllowedLength), ')', ', ')
				END

			SET @WhereClause = CONCAT(@WhereClause, '[', @COLUMN_NAME, '] = @Where', @COLUMN_NAME, ' AND ')
		
			FETCH NEXT FROM CurQuery INTO @COLUMN_NAME, @DATA_TYPE, @AllowedLength
		END
	CLOSE CurQuery
	DEALLOCATE CurQuery

		SET @ParamsForWhereClause = LEFT(@ParamsForWhereClause, LEN(@ParamsForWhereClause) - 1)
		SET @WhereClause = LEFT(@WhereClause, LEN(@WhereClause) - 4)
	
		DECLARE CursorColumns CURSOR FOR
			SELECT [COLUMN_NAME], [DATA_TYPE], [CHARACTER_MAXIMUM_LENGTH]
				FROM [INFORMATION_SCHEMA].[COLUMNS]
					WHERE [TABLE_SCHEMA] LIKE @SCHEMA_NAME
						AND [TABLE_NAME] LIKE @TABLE_NAME
	
		DECLARE @ParametersForProcedure nvarchar(MAX)
			DECLARE @ColumnsOfTable nvarchar(MAX)
				DECLARE @VALUES nvarchar(MAX)
					DECLARE @SetClause NVARCHAR(MAX)

	
	OPEN CursorColumns
		FETCH NEXT FROM CursorColumns INTO @COLUMN_NAME, @DATA_TYPE, @AllowedLength
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (COLUMNPROPERTY(
					OBJECT_ID(@SCHEMA_NAME + '.' + @TABLE_NAME), @COLUMN_NAME, 'IsIdentity') = 0)
			BEGIN
				IF (@AllowedLength IS NULL)
				BEGIN
					SET @ParametersForProcedure = 
							CONCAT(@ParametersForProcedure, '@', @COLUMN_NAME, ' ', @DATA_TYPE, ', ')
				END
				ELSE IF (@AllowedLength = -1)
				BEGIN
					SET @ParametersForProcedure = 
							CONCAT(@ParametersForProcedure, '@', @COLUMN_NAME, ' ', @DATA_TYPE, '(MAX)', ', ')
				END
				ELSE
				BEGIN
					SET @ParametersForProcedure = 
							CONCAT(@ParametersForProcedure, '@', @COLUMN_NAME, ' ', @DATA_TYPE, '(', 
								CONVERT(VARCHAR(10), @AllowedLength), ')', ', ')
				END


				--Concatenar Colunas pertencentes � tabela
				SET @ColumnsOfTable = 
						CONCAT(@ColumnsOfTable, '[', @COLUMN_NAME, ']', ', ')

				--Concatenar Campos pertencentes �s colunas
				SET @VALUES = 
						CONCAT(@VALUES, '@', @COLUMN_NAME, ', ')

				--Concatenar Clausulas pertencentes �s colunas
				SET @SetClause = 
					CONCAT(@SetClause, '[', @COLUMN_NAME, '] = ', '@', @COLUMN_NAME, ', ')

			END
		
			FETCH NEXT FROM CursorColumns INTO @COLUMN_NAME, @DATA_TYPE, @AllowedLength
		END
	CLOSE CursorColumns
	DEALLOCATE CursorColumns

	-- Retirar a ultima virgula (depois de todos os par�metros)
	SET @ParametersForProcedure = LEFT(@ParametersForProcedure, LEN(@ParametersForProcedure) - 1)
		SET @ColumnsOfTable = LEFT(@ColumnsOfTable, LEN(@ColumnsOfTable) - 1)
			SET @VALUES = LEFT(@VALUES, LEN(@VALUES) - 1)
				SET @SetClause = LEFT(@SetClause, LEN(@SetClause) - 1)


	EXEC [SWS].[sp_generateInsertProcedure] @TABLE_NAME, @SCHEMA_NAME, @ParametersForProcedure,
	  @ColumnsOfTable, @VALUES

	EXEC [SWS].[sp_generateUpdateProcedure] @TABLE_NAME, @SCHEMA_NAME, @ParametersForProcedure,
			@ParamsForWhereClause, @ColumnsOfTable, @WhereClause, @VALUES,
				@SetClause 

	EXEC [SWS].[sp_generateDeleteProcedure] @TABLE_NAME, @SCHEMA_NAME,
	 @ParamsForWhereClause, @WhereClause

	END