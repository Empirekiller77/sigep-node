var kue = require("./kue");
var Queue = kue.Queue;
var nodemailer = require("nodemailer");
var Hogan = require("hogan.js");
var fs = require('fs');
//getfile
var template = fs.readFileSync('scripts/email-template.hjs', 'utf-8');
//compile template
var compiledTemplate = Hogan.compile(template);

Queue.process("resendEmail", async function(job, done) {
  let { data } = job;
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    type: "SMTP",
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
      user: 'suggestions.nahda@gmail.com',
      pass: 'sigep2019'
    }
  });
  
  var mailOptions = {
    from: 'suggestions.nahda@gmail.com',
    to: data.receiver_user_number+'@estudantes.ips.pt',
    subject: "SIGEP Notification",
    attachments: [{
      filename: 'logoLP.png',
      path: './images/logoLP.png',
      cid: 'cid: unique@kreata.ee'
 }],
    html: compiledTemplate.render({origin_user_name: data.origin_user_name,
                                  receiver_user_name:data.receiver_user_name,
                                  notification_format_text:data.notification_format_text})
  };
  
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent');
      
    }
  });
  done();
});
