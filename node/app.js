"use strict";
const express = require("express");
const requestHandlers = require("./scripts/request-handlers.js");
const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(express.static("www"));

//emails
app.route("/notify_user")
.post(requestHandlers.sendNotificationEmail);

app.route("/email")
.post(requestHandlers.sendEmail);

app.route("/testSelect")
.post(requestHandlers.testSelect);

app.listen(8086, function() {
    console.log("Server running at http://localhost:8086");
});