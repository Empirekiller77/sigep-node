﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using SIGEP.Controllers;
using SIGEP.Data;
using SIGEP.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Xunit;

namespace testSIGEP
{
    public class UserControllerTest
    {
        [Fact]
        public void StudentsTest()
        {
            var connection = new SqlConnection("Data Source=LAPTOP-TU53V2D4;Initial Catalog =SWDB; Integrated Security=True; Password = Password");
            connection.Open();
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlServer(connection)
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var controller = new UsersController(null, context);
                var result = controller.Students();
                var viewResult = Assert.IsType<ViewResult>(result);
            }
        }

        [Fact]
        public void AdvisorsTest()
        {
            var connection = new SqlConnection("Data Source=LAPTOP-TU53V2D4;Initial Catalog =SWDB; Integrated Security=True; Password = Password");
            connection.Open();
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlServer(connection)
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var controller = new UsersController(null, context);
                var result = controller.Advisors();
                var viewResult = Assert.IsType<ViewResult>(result);
            }
        }




        [Fact]
        public void Perfil()
        {
            var connection = new SqlConnection("Data Source=LAPTOP-TU53V2D4;Initial Catalog =SWDB; Integrated Security=True; Password = Password");
            connection.Open();
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlServer(connection)
                .Options;
            using (var context = new ApplicationDbContext(options))
            {


                var controller = new UsersController(null, context);
               
                var result = controller.Perfil();
                var viewResult = Assert.IsType<ViewResult>(result);
                var viewbagName = controller.ViewBag.name;
                var viewbagUserNumber = controller.ViewBag.user_number;
                var viewbagEmail = controller.ViewBag.email;
                Assert.Equal(true, viewbagName != null);
                Assert.Equal(true, viewbagUserNumber != null);
                Assert.Equal(true, viewbagEmail != null);

            }

        }


    }
}
